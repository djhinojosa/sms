<!DOCTYPE html>

<html lang="en" >
<!-- begin::Head -->
<head>
	<meta charset="utf-8"/>

	<title>SwrdTech | Acceso</title>
	<meta name="description" content="Login page example">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Asap+Condensed:500">        <!--end::Fonts -->


	<!--begin::Page Custom Styles(used by this page) -->
	<link href="./assets/css/demo8/pages/general/login/login-2.css" rel="stylesheet" type="text/css" />
	<!--end::Page Custom Styles -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="./assets/vendors/global/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="./assets/css/demo8/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<!--end::Layout Skins -->

	<link rel="shortcut icon" href="./assets/media/logos/favicon.ico" />
	<style> 
	input[type=text] {
		color: white;
	}
	input[type=password] {
		color: white;
	}
	input[type=text]:focus {
		color: white;
	}
	input[type=password]:focus {
		color: white;
	}
</style>

</head>
<!-- end::Head -->

<!-- begin::Body -->
<body  style="background-image: url(./assets/media/demos/demo8/bg-1.jpg)"  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

	<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
		<div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(./assets/media//bg/bg-1.jpg);">
				<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
					<div class="kt-login__container">
						<div class="kt-login__logo">
							<a href="#">
								<img src="./img/logo.png" width="45%">  	
							</a>
						</div>
						<div class="kt-login__signin" style="margin-top: -40px;">
							<div class="kt-login__head">
								<h3 class="kt-login__title">Sistema de Inventario</h3>
							</div>
							<form class="kt-form" action="" style="margin-top: -10px;">
								<div class="input-group">
									<input class="form-control" type="text" placeholder="Usuario" name="email" autocomplete="off">
								</div>
								<div class="input-group">
									<input class="form-control" type="password" placeholder="Contraseña" name="password">
								</div>

								<div class="kt-login__actions ">
									<button id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">Acceder</button>
								</div>
							</form>
						</div>


					</div>	
				</div>
			</div>
		</div>	
	</div>
	
	<!-- end:: Page -->

	<!-- end::Global Config -->
	<!--begin::Global Theme Bundle(used by all pages) -->
	<script src="./assets/vendors/global/vendors.bundle.js" type="text/javascript"></script>
	<script src="./assets/js/demo8/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Global Theme Bundle -->
	<!--begin::Page Scripts(used by this page) -->
	<script src="./assets/js/demo8/pages/login/login-general.js" type="text/javascript"></script>
	<!--end::Page Scripts -->
	<script src="./js/validacion.js" type="text/javascript"></script>
</body>
<!-- end::Body -->
</html>
