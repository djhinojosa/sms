@extends('layout.master')
@section('content')

<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Registro de Incidencias
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
					<!-- <a href="#" class="btn btn-clean btn-icon-sm">
						<i class="la la-long-arrow-left"></i>
						Rgresar
					</a>
					&nbsp;

					<div class="dropdown dropdown-inline">
						
							<button type="button" class="btn btn-brand btn-icon-sm" data-toggle="modal" data-target="#kt_modal_4" >
								<i class="flaticon2-plus"></i> Agregar Nuevo     
							</button>

						
						</div> -->
					</div>    
				</div>
			</div>

			<div class="kt-portlet__body">
				<!--begin: Search Form -->

				<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
					<form action="incidencias" method="post">
						{{csrf_field()}}
						<div class="form-group form-group-last row">
							<div class="form-group col-lg-3">
								<label for="recipient-name" class="form-control-label">Fecha:</label>
								<input type="text" class="form-control" id="kt_datepicker_1" readonly placeholder="Seleccione la fecha" maxlength="0"  />

							</div>
							<div class="form-group col-lg-3">
								<label for="recipient-name" class="form-control-label">Hora:</label>

								<input type="text" name="hora" id="hora" class="form-control"  onblur="return val.uppercase(this.value,this.id)">
							</div>
							<div class="form-group col-lg-3">
								<label for="recipient-name" class="form-control-label">Estación/Unidad:</label>

								<input type="text" name="estacion" id="estacion" class="form-control"  onblur="return val.uppercase(this.value,this.id)">
							</div>
							<div class="form-group col-lg-3">
								<label for="recipient-name" class="form-control-label">Localidad:</label>

								<input type="text" name="localidad" id="localidad" class="form-control"  onblur="return val.uppercase(this.value,this.id)">
							</div>
						</div>
						<div class="form-group">
							<label for="message-text" class="form-control-label">Que Observó Usted?:</label>
							<textarea class="form-control" id="observacion" name="observacion" onblur="return val.uppercase(this.value,this.id)"></textarea>
						</div>
						<div class="form-group form-group-last row">
							<div class="form-group col-lg-4">

								
								<label for="recipient-name" class="form-control-label">Observación de Seguridad(OS):</label>
								<select class="form-control" id="OS" name="OS" >
									<option value="">Seleccione</option>
									<option value="SI">SI</option>
									<option value="NO">NO</option>
								</select>

							</div>
							<div class="form-group col-lg-4">
								<label for="recipient-name" class="form-control-label">Casi Accidente(NM):</label>

								<select class="form-control" id="CA" name="CA" >
									<option value="">Seleccione</option>
									<option value="SI">SI</option>
									<option value="NO">NO</option>
								</select>
								
							</div>
							<div class="form-group col-lg-4">
								<label for="recipient-name" class="form-control-label">Hablo Ud. con la Persona?(si involucró a personas):</label>
								<select class="form-control" id="interaccion" name="interaccion" >
									<option value="">Seleccione</option>
									<option value="SI">SI</option>
									<option value="NO">NO</option>
								</select>
								
							</div>
							

						</div>
						<div class="form-group">
							<label for="message-text" class="form-control-label">Que Medidas se tomaron inmediatamente?:</label>
							<textarea class="form-control" id="medidas" name="medidas" onblur="return val.uppercase(this.value,this.id)"></textarea>
						</div>
						<!--begin::Form-->

						<div class="form-group form-group-last row">
							<div class="form-group col-lg-6">

								<div class="kt-checkbox-list">
									<label class="kt-checkbox">
										<input type="checkbox" id="RS" name="RS"> Reportar al Superior
										<span></span>
									</label>

									<label class="kt-checkbox">
										<input type="checkbox" id="BT" name="BT"> Brief en el turno
										<span></span>
									</label>
									<label class="kt-checkbox">
										<input type="checkbox" id="SMT" name="SMT"> Registro en SMT
										<span></span>
									</label>

								</div>


							</div>
							<div class="form-group col-lg-6">

								<div class="kt-checkbox-list">
									<label class="kt-checkbox">
										<input type="checkbox" id="PI" name="PI"> Promover investigar
										<span></span>
									</label>
									<label class="kt-checkbox">
										<input type="checkbox" id="RE" name="RE"> Recompensar a Empleado
										<span></span>
									</label>
									<label class="kt-checkbox">
										<input type="checkbox" id="RAL" name="RAL"> Registro con Autoridad local
										<span></span>
									</label>
									<label class="kt-checkbox">
										<input type="checkbox" id="RA" name="RA"> Reportar a Aerolinea
										<span></span>
									</label>

								</div>


							</div>


						</div>
						
						
						<button type="submit" class="btn btn-brand btn-icon-sm" >
							<i class="flaticon2-plus"></i> Registrar      
						</button>
					</form>
				</div>		
				<!--end: Search Form -->
			</div>


		</div>	


		@endsection
		@section('scripts')	
		<!--begin::Page Scripts(used by this page) -->
		<script type="text/javascript">
			$( document ).ready(function() {
				$('#MenuDistribu').attr('class', 'kt-menu__item  kt-menu__item--active kt-menu__item--submenu kt-menu__item--rel');
			});
		</script>

		<!--begin::Page Scripts(used by this page) -->
		<script src="./assets/js/demo8/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="./assets/vendors/general/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js" type="text/javascript"></script>
		<!--end::Page Scripts -->

		@endsection