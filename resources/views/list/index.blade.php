@extends('layout.master')
@section('content')



<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
			<h3 class="kt-portlet__head-title">
				Listado de Incidencias
			</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
					<!-- <a href="#" class="btn btn-clean btn-icon-sm">
						<i class="la la-long-arrow-left"></i>
						Rgresar
					</a>
					&nbsp; -->
					<div class="dropdown dropdown-inline">

						
						<a href="./incidencias">
							<button type="button" class="btn btn-brand btn-icon-sm"  >
								<i class="flaticon2-plus"></i> Agregar Nueva Incidencia     
							</button>
						</a>	


					</div>

				</div>    
			</div>
		</div>

		<div class="kt-portlet__body">
			<!--begin: Search Form -->
			<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="row align-items-center">				
							<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
								<div class="kt-input-icon kt-input-icon--left">
									<input type="text" class="form-control" placeholder="Buscar..." id="generalSearch">
									<span class="kt-input-icon__icon kt-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
							

						</div>
					</div>
					<div class="col-xl-4 order-1 order-xl-2 kt-align-right">
						
						<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
					</div>
				</div>
			</div>		
			<!--end: Search Form -->
		</div>

		<div class="kt-portlet__body kt-portlet__body--fit">
			<!--begin: Datatable -->
			<table class="kt-datatable" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1">Fecha</th>
						<th title="Field #2">Hora</th>
						<th title="Field #3">Estación</th>
						<th title="Field #4">Localidad</th>
						<th title="Field #5">Observación</th>
						<th title="Field #6">Acción</th>
						<th title="Field #7">Medidas</th>
						<th title="Field #8">Pasos</th>
						<th title="Field #9">Observador</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($incidencias as $key): ?>
						<tr>
							<td><?php echo $key->fecha ?></td>
							<td><?php echo $key->hora ?></td>
							<td><?php echo $key->estacion ?></td>
							<td><?php echo $key->localidad ?></td>
							<td><?php echo $key->observacion ?></td>
							<td>
								<?php if ($key->OS != ''): ?>
									Observación de Seguridad(OS): <?php echo $key->OS ?>
									<br>
								<?php endif ?>
								<?php if ($key->CA != ''): ?>
									Casi Accidente(NM): <?php echo $key->CA ?>
									<br>
								<?php endif ?>
								<?php if ($key->interaccion != ''): ?>
									Hablo con la Persona?: <?php echo $key->interaccion ?>
									<br>
								<?php endif ?>
								

							</td>
							<td><?php echo $key->medidas ?></td>
							<td>
								<?php if ($key->Rp != ''): ?>
									Reportar al Superior: 
									<?php if ($key->Rp == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->Rp == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>
								<?php if ($key->BT != ''): ?>
									Brief en el turno: 
									<?php if ($key->BT == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->BT == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>
								<?php if ($key->SMT != ''): ?>
									Registro en SMT: 
									<?php if ($key->SMT == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->SMT == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>
								<?php if ($key->PI != ''): ?>
									Promover investigar: 
									<?php if ($key->PI == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->PI == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>
								<?php if ($key->RE != ''): ?>
									Recompensar a Empleado: 
									<?php if ($key->RE == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->RE == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>
								<?php if ($key->RAL != ''): ?>
									Registro con Autoridad local: 
									<?php if ($key->RAL == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->RAL == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>
								<?php if ($key->RA != ''): ?>
									Reportar a Aerolinea: 
									<?php if ($key->RA == TRUE): ?>
										SI
									<?php endif ?>
									<?php if ($key->RA == FALSE): ?>
										NO
									<?php endif ?>
									<br>
								<?php endif ?>



							</td>
							<td><?php echo $key->username ?></td>

						</tr>
					<?php endforeach ?>



				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>	



	@endsection
	@section('scripts')	
	<!--begin::Page Scripts(used by this page) -->
	<script type="text/javascript">
		$( document ).ready(function() {
			$('#MenuDistribu').attr('class', 'kt-menu__item  kt-menu__item--active kt-menu__item--submenu kt-menu__item--rel');
		});
	</script>

	<script src="./js/mercancia.js" type="text/javascript"></script>
	<!--:  -->
	<!--: Datatable -->

	<script src="./assets/js/demo8/pages/crud/metronic-datatable/base/data-mercancia.js" type="text/javascript"></script>
	@endsection