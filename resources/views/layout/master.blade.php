<!DOCTYPE html>

<html lang="en" >
<!-- begin::Head -->
<head>
  <meta charset="utf-8"/>

  <title>SwrdTech | SCI</title>
  <meta name="description" content="Datatable HTML table">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!--begin::Fonts -->
      <!--end::Fonts -->


  @include('layout.resources.libreria_tabla')
  @yield('css')

  <link rel="shortcut icon" href="./img/logo.png" />
  <!-- Hotjar Tracking Code for keenthemes.com -->


</head>
<!-- end::Head -->

<!-- begin::Body -->
<body  style="background-image: url(./assets/media/demos/demo8/bg-1.jpg)"  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >


 <!-- begin:: Page -->
 <!-- begin:: Header Mobile -->
 <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed " >
   <div class="kt-header-mobile__logo">
    <img alt="Logo" src="./img/logo.png" width="20%" />
  </div>
  <div class="kt-header-mobile__toolbar">

    <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
    <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
  </div>
</div>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
  <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
   <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
    <!-- begin:: Header -->
    <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed "  data-ktheader-minimize="on" >
     <div class="kt-header__top">
      <div class="kt-container ">
       <!-- begin:: Brand -->
       <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
         <div class="kt-header__brand-logo">

          <img alt="Logo" src="./img/logo.png" class="kt-header__brand-logo-default" width="7%" />
          <img alt="Logo" src="./img/logo.png" class="kt-header__brand-logo-sticky" width="7%" />		

        </div> 
      </div>
      <!-- end:: Brand -->	
      <!-- begin:: Header  Topbar -->
      @include('layout.resources.head')
      <!-- end:: Header Topbar -->
    </div>
  </div>
  <div class="kt-header__bottom">
    <div class="kt-container ">
     <!-- begin: Header Menu -->
     @include('layout.resources.menu')
     <!-- end: Header Menu -->	
   </div>
 </div>
</div>
<!-- end:: Header -->
<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
 <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
   <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
    <!-- begin:: Content -->
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

      <br><br>
      <div id="DivMainContent">
        @yield('content')
      </div>
    </div>
    <!-- end:: Content -->					
  </div>
</div>
</div>

<!-- begin:: Footer -->
@include('layout.resources.footer')
<!-- end:: Footer -->		
</div>
</div>
</div>

<!-- end:: Page -->


<!--begin::Page -->
@include('layout.resources.libreria_js')
@yield('scripts')
<!--end::Page Scripts -->
</body>
<!-- end::Body -->
</html>
