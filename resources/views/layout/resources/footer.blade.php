  <div class="kt-footer kt-grid__item" id="kt_footer">
    <div class="kt-container">
        <div class="kt-footer__wrapper">
            <div class="kt-footer__copyright">
                2019&nbsp;&copy;&nbsp;<a href="#" target="_blank" class="kt-link">SwrdTech</a>
                <!-- http://keenthemes.com/metronic -->
            </div>
        </div>
    </div>
</div>