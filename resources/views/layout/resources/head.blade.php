<div class="kt-header__topbar">


 <!--begin: Notifications -->
 <div class="kt-header__topbar-item dropdown">
  <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,10px">
   <span class="kt-header__topbar-icon" >
    <i class="flaticon2-bell-alarm-symbol"></i>
    <span class="kt-badge kt-badge--success kt-hidden"></span>
  </span>
</div>
<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
 <form>
  <!--begin: Head -->
  <div class="kt-head kt-head--skin-light kt-head--fit-x kt-head--fit-b">
    <h3 class="kt-head__title">
      Notificaciones
      &nbsp;
      <span class="btn btn-label-primary btn-sm btn-bold btn-font-md"> Recepciones Pendientes</span>
    </h3>
    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
      <li class="nav-item">
        <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
      </li>
    </ul>
  </div>
  <!--end: Head -->
  <div class="tab-content">
    <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
      <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">



      </div>
    </div>
  </div>
  <!--end: Head -->
</form>
</div>
</div>
<!--end: Notifications -->

<!--begin: User bar -->
<div class="kt-header__topbar-item kt-header__topbar-item--user">
  <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,10px">
    <span class="kt-header__topbar-welcome kt-visible-desktop">Bienvenido,</span>
    <span class="kt-header__topbar-username kt-visible-desktop">{{Auth::user()->username}}</span>
    <img alt="Pic" src="./assets/media/users/300_21.jpg"/>
    <span class="kt-header__topbar-icon kt-bg-brand kt-font-lg kt-font-bold kt-font-light kt-hidden">S</span>
    <span class="kt-header__topbar-icon kt-hidden"><i class="flaticon2-user-outline-symbol"></i></span>
  </div>
  <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
   <!--begin: Head -->
   <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
    <div class="kt-user-card__avatar">
      <img class="kt-hidden-" alt="Pic" src="./assets/media/users/300_25.jpg" />
      <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
      <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
    </div>
    <div class="kt-user-card__name">
      {{Auth::user()->name.' '.Auth::user()->surname}} 
    </div>
   <!-- <div class="kt-user-card__badge">
        <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>
    </div>
  -->
</div>

<!--end: Head -->

<!--begin: Navigation -->
<div class="kt-notification">
   <!-- <a href="/metronic/preview/demo8/custom/apps/user/profile-1/personal-information.html" class="kt-notification__item">
        <div class="kt-notification__item-icon">
            <i class="flaticon2-calendar-3 kt-font-success"></i>
        </div>
        <div class="kt-notification__item-details">
            <div class="kt-notification__item-title kt-font-bold">
                My Profile
            </div>
            <div class="kt-notification__item-time">
                Account settings and more
            </div>
        </div>
      </a>-->

      <div class="kt-notification__custom kt-space-between">
        <a href="./logout"  class="btn btn-label btn-label-brand btn-sm btn-bold">Desconectarme</a>


      </div>
    </div>
    <!--end: Navigation -->
  </div>
</div>
<!--end: User bar -->

</div>