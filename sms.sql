create table incidencias (
    id int not null auto_increment primary key,
    obsv text not null,
    tipo_obsv enum('OS', 'NM'),
    hablo_persona enum('si', 'no'),
    medidas text not null,
    fecha datetime not null,
    id_user int not null
) engine = innodb;

create table adj_imgs (
    id int not null auto_increment primary key,
    folder varchar(255) not null,
    id_incidencia int not null
) engine = innodb;

create table users (
    id int not null auto_increment primary key,
    username varchar(100) unique not null,
    password varchar(100) not null,
    name varchar(50) not null,
    surname varchar(50) not null,
    email varchar (150) unique not null
) engine = innodb;