// Class definition
var KTFormRepeater = function() {



    var demo3 = function() {
        $('#kt_repeater_3').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();                               
            },

            hide: function(deleteElement) {                 
                if(confirm('Esta seguro que desea eliminar esta línea?')) {
                    $(this).slideUp(deleteElement);
                }                                  
            }      
        });
    }

    


    return {
        // public functions
        init: function() {
            
            demo3();
            
        }
    };
}();

jQuery(document).ready(function() {
    KTFormRepeater.init();
});

