"use strict";

// Class Definition
var KTLoginGeneral = function() {

    var login = $('#kt_login');


    var handleSignInFormSubmit = function() {
        $('#kt_login_signin_submit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    email: {
                        required: true,
                    },
                    password: {
                        required: true,
                        
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            var token = $("#token").val();
            form.ajaxSubmit({
                headers: {'X-CSRF-TOKEN':token},
                url: 'login',
                type: 'post',
                success: function(response, status, xhr, $form) {
                	// similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        val.showErrorMsg(form, 'success', 'Bienvenido. Accediendo al sistema.');
                    }, 2000);
                    setTimeout(function() {
                        window.location = './principal';
                    }, 3000);

                },    
                error: function(response, status, xhr, $form) {
                    // similate 2s delay
                    setTimeout(function() {
                        btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        val.showErrorMsg(form, 'danger', 'Los datos ingresados son incorrectos. porfavor vuelva a intentar.');
                    }, 2000);
                }
            });
        });
    }



    // Public Functions
    return {
        // public functions
        init: function() {
            handleSignInFormSubmit();
        }
    };
}();

// Class Initialization
jQuery(document).ready(function() {
    KTLoginGeneral.init();
});