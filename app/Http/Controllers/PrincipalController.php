<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;


class PrincipalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //


        return view('home.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Consulta del listado
     /*   $mercancia= TeMercancia::
        leftJoin('ti_categoria', 'ti_categoria.clvcategoria', '=', 'te_mercancia.clvcategoria')
        ->leftJoin('ti_marca', 'ti_marca.clvmarca', '=', 'te_mercancia.clvmarca')
        ->leftJoin('ti_modelo', 'ti_modelo.clvmodelo', '=', 'te_mercancia.clvmodelo')
        ->leftJoin('ti_color', 'ti_color.clvcolor', '=', 'te_mercancia.clvcolor')
        ->select('ti_categoria.strdescripcion as categoria', 'ti_marca.strdescripcion as marca', 'ti_modelo.strdescripcion as modelo', 'ti_color.strdescripcion as color', 'te_mercancia.strdescripcion as especificacion', 'te_mercancia.clvmercancia', 'te_mercancia.strcodigo')
        ->where('te_mercancia.blnborrado', false)
        ->get();
        $data= array();
        foreach ($mercancia as $key ) {
            # code...
            $invtario = TeInventario::where('clvmercancia', $key->clvmercancia)->get();
            $stock = 0;
            $stockS = 0;
            foreach ($invtario as $value) {
                # code... consulta del inventario de la empresa principal para mostrar en la bandeja
                if ($value->clvempresa == '') {
                   $stock = $stock + $value->cantidad;
               }else{
                 $stockS = $stockS + $value->cantidad;
             }
         }
         $data [$key->clvmercancia] = 
         array(
             'Codigo' => $key->strcodigo,
             'Categoria' => $key->categoria,
             'Marca' => $key->marca,
             'Modelo' => $key->modelo,
             'Especificacion' => $key->especificacion,
             'Color' => $key->color,
             'Stock' => $stock,
             'StockSucursal' => $stockS,
             'Opcion' => '<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#kt_modal_5" onclick="producto.ProcesarProducto('.$key->clvmercancia.');"><i class="flaticon-eye"></i></button>',
         );

     }

     return response()->json($data);
     */

 }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        switch ($request->case) {
            case 1:
            //registro del nuevo producto
            $clvmarca='';
            $marca='';
            $clvmodelo='';
            $modelo='';
            if ($request->data[1] == 'otro') {
                # code...  //registro de  nueva marca o modelo
             $marca  = TiMarca::insert(array(
                'strdescripcion' => $request->data[5]
            ));
             //consulta para generar el codigo
             $marca_consult  = TiMarca::where('strdescripcion', $request->data[5])->first();
             $clvmarca=$marca_consult->clvmarca;
             $marca=$marca_consult->strdescripcion;
         }else{//en caso de existir la marca o modelo, rellana variables para generar el codigo
            $marca_consult  = TiMarca::where('clvmarca', $request->data[1])->first();
            $clvmarca=$marca_consult->clvmarca;
            $marca=$marca_consult->strdescripcion;
        }

        if ($request->data[2] == 'otro') {
            # code...  //registro de  nueva marca o modelo
         $modelo = TiModelo::insert(array(
            'strdescripcion' => $request->data[6],
            'clvmarca' => $clvmarca
        ));
         //consulta para generar el codigo
         $modelo_consult = TiModelo::where('strdescripcion', $request->data[6])->first();
         $clvmodelo=$modelo_consult->clvmodelo;
         $modelo=$modelo_consult->strdescripcion;
     }else{//en caso de existir la marca o modelo, rellana variables para generar el codigo
        $modelo_consult = TiModelo::where('clvmodelo', $request->data[2])->first();
        $clvmodelo=$modelo_consult->clvmodelo;
        $modelo=$modelo_consult->strdescripcion;
    }

            //consultas para generar el codigo
    $categoria = TiCategoria::where('clvcategoria', $request->data[0])->first();
    $color = TiColor::where('clvcolor', $request->data[3])->first();
            //creacion de las variables para el codigo
    $categoria = str_split($categoria->strdescripcion, 1);
    $marca = str_split($marca, 3);
    $modelo = str_split($modelo, 3);
    $color= str_split($color->strdescripcion, 3);
            //codigo del producto
    $codigo= $categoria[0].'-'.$marca[0].$modelo[0].$color[0];
            //consulta de la marca y modelo registrado

            //array para enviar al modelo
    $data = array(
        'strdescripcion' => $request->data[4],
        'strcodigo' => $codigo,
        'clvmodelo' => $clvmodelo, 
        'clvmarca' => $clvmarca, 
        'clvcolor' => $request->data[3], 
        'clvcategoria' => $request->data[0], 
    );
    $mercancia = TeMercancia::InsertProducto($data);
    echo 'Registro Exitoso';
    break;
    case 2:
    //registro de stock en el inventario
    $cantidad_total=$request->data[1]+$request->data[2];
    $data = array(
        'cantidad' =>  $cantidad_total, 
        'strobservacion' => 'NINGUNA', 
        'clvempresa' => 1, 
        'clvcondicion' => 1, 
        'clvmercancia' => $request->data[0], 
    );
    $inventario = TeInventario::InsertInventario($data);
    echo 'Registro Exitoso';
    break;
    case 3:


    $producto= TeMercancia::where('clvmercancia', $request->data[5])->first();

    //actualizar marca
    TiMarca::where('clvmarca', $producto->clvmarca)
    ->update([
        'strdescripcion'  => $request->data[1]
    ]);
    //actualizar modelo
    TiModelo::where('clvmodelo', $producto->clvmodelo)
    ->update([
        'strdescripcion'  => $request->data[2]
    ]);


    $marca_consult  = TiMarca::where('clvmarca', $producto->clvmarca)->first();
    $marca=$marca_consult->strdescripcion;
    $modelo_consult = TiModelo::where('clvmodelo', $producto->clvmodelo)->first();
    $clvmodelo=$modelo_consult->clvmodelo;
    $modelo=$modelo_consult->strdescripcion;
    //consultas para generar el codigo
    $categoria = TiCategoria::where('clvcategoria', $request->data[0])->first();
    $color = TiColor::where('clvcolor', $request->data[3])->first();
            //creacion de las variables para el codigo
    $categoria = str_split($categoria->strdescripcion, 1);
    $marca = str_split($marca, 3);
    $modelo = str_split($modelo, 3);
    $color= str_split($color->strdescripcion, 3);
            //codigo del producto
    $codigo= $categoria[0].'-'.$marca[0].$modelo[0].$color[0];
            //consulta de la marca y modelo registrado
    //actualizar especificacion
    TeMercancia::where('clvmercancia', $request->data[5])
    ->update([
        'clvcategoria'  => $request->data[0],
        'clvcolor'  => $request->data[3],
        'strdescripcion'  => $request->data[4],
        'strcodigo' => $codigo,
    ]);

    echo 'Actualización Exitosa';
    break;


}
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        switch ($request->case) {
            case 1:
                # code... Registro de nuevos productos
            //consulta del producto
            $mercancia= TeMercancia::
            leftJoin('ti_categoria', 'ti_categoria.clvcategoria', '=', 'te_mercancia.clvcategoria')
            ->leftJoin('ti_marca', 'ti_marca.clvmarca', '=', 'te_mercancia.clvmarca')
            ->leftJoin('ti_modelo', 'ti_modelo.clvmodelo', '=', 'te_mercancia.clvmodelo')
            ->leftJoin('ti_color', 'ti_color.clvcolor', '=', 'te_mercancia.clvcolor')
            ->select('ti_categoria.strdescripcion as categoria', 'ti_marca.strdescripcion as marca', 'ti_modelo.strdescripcion as modelo', 'ti_color.strdescripcion as color', 'te_mercancia.strdescripcion as especificacion', 'te_mercancia.clvmercancia')
            ->where('te_mercancia.clvmercancia', $request->data)
            ->first();

            //consulta de las sucursales

            $sucursales = TeEmpresa::where('blnborrado', false)->get();
            //consulta de inventario separada para unir por la empresa en la vista
            $inventario = TeInventario::where('blnborrado', false)->get();

            return view('mercancia.resources.inventario')
            ->with('sucursales', $sucursales)
            ->with('inventario', $inventario)
            ->with('producto', $mercancia);
            break;
            case 2:
                # code... Registro de nuevos productos
            //consulta del producto
            $mercancia= TeMercancia::
            leftJoin('ti_categoria', 'ti_categoria.clvcategoria', '=', 'te_mercancia.clvcategoria')
            ->leftJoin('ti_marca', 'ti_marca.clvmarca', '=', 'te_mercancia.clvmarca')
            ->leftJoin('ti_modelo', 'ti_modelo.clvmodelo', '=', 'te_mercancia.clvmodelo')
            ->leftJoin('ti_color', 'ti_color.clvcolor', '=', 'te_mercancia.clvcolor')
            ->select('ti_categoria.clvcategoria as categoria', 'ti_marca.strdescripcion as marca', 'ti_modelo.strdescripcion as modelo', 'ti_color.clvcolor as color', 'te_mercancia.strdescripcion as especificacion', 'te_mercancia.clvmercancia')
            ->where('te_mercancia.clvmercancia', $request->data)
            ->first();

            $categoria = TiCategoria::where('blnborrado', false)->get();
            $color = TiColor::where('blnborrado', false)->get();
            return view('mercancia.resources.update')
            ->with('categoria', $categoria)
            ->with('color', $color)
            ->with('producto', $mercancia);
            break;
            

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
