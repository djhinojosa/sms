<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/principal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request)
    {

  //Variable que se utiliza para el proceso de logueo de laravel
        $credentials = [
            'username' => strtoupper($request->email),
            'password' => $request->password,
        ];

        //Condicional if que verifica si el usuario se puede loguear exitosamente
        if (Auth::attempt($credentials)) {

           return  Response::json(array(
            'code'      =>  200,
            'message'   =>  'success'
        ), 200) ;

       } else {
           return  Response::json(array(
            'code'      =>  400,
            'message'   =>  'Error'
        ), 400) ;
       }

   }

   public function logout()
   {
    Auth::logout();
    return redirect('/');
}
}
