<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class IncidenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('incidencias.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        DB::table('incidencia')->insert(array(
            'fecha' => $request->kt_datepicker_1,
            'hora' => $request->hora,
            'estacion' => $request->estacion,
            'localidad' => $request->localidad,
            'observacion' => $request->observacion,
            'OS' => $request->OS,
            'CA' => $request->CA,
            'interaccion' => $request->interaccion,
            'medidas' => $request->medidas,
            'Rp' => $request->RS,
            'BT' => $request->BT,
            'SMT' => $request->SMT,
            'PI' => $request->PI,
            'RE' => $request->RE,
            'RAL' => $request->RAL,
            'RA' => $request->RA,
            'id_user' => 1

        ));

        return redirect('/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
